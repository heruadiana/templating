<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('halaman.biodata');
    }

    public function welcome(Request $request){
        $nama = $request ->nama;
        return view('halaman.ketiga', compact('nama'));
    }
}
