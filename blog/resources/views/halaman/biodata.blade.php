@extends('layout.master')

@section('judul')
Halaman Biodata
@endsection

@section('content')
<h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action='/welcome' method="post">
        @csrf
        <label>Nama Lengkap:</label><br><br>
        <input type="text" name="nama"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" id="male" name="gender"><label for="male">Male</label><br>
        <input type="radio" id="female" name="gender"><label for="female">Female</label><br>
        <input type="radio" id="other" name="gender"><label for="other">Other</label><br><br>
        <label>Nationality:</label><br><br>
        <select>
            <option>Indonesia</option>
            <option>Palestina</option>
            <option>Amerika</option>
        </select><br><br>
        <label>Language Spoken</label><br><br>
        <input type="checkbox" id="indo"><label for="indo">Bahasa Indonesia</label><br>
        <input type="checkbox" id="english"><label for="english">English</label><br>
        <input type="checkbox" id="other"><label for="other"> Other</label><br><br>
        <label>Bio:</label><br><br>
        <textarea name="bio" rows="10" cols="30"></textarea><br><br>
        <button type="submit" value="welcome">Sign Up</button>
    </form>
@endsection